// import express library
const express = require("express");

// create app express (khoi tao ung dung express)
const app = express();

// khai bao port tren he thong
const port = 8000;

// khai bao API
app.get("/", (req, res) => {
    let today = new Date();

    // response tra ve 1 chuoi 200
    res.status(200).json({
        message: `Hello, today is ${today.getDate()} ${today.getMonth()+1}, ${today.getFullYear()}`
    })
});

// GET method
app.get("/get-method", (req, res) => {
    res.status(200).json({
        message: `GET method`
    })
});

// POST method
app.post("/post-method", (req, res) => {
    res.status(200).json({
        message: `POST method`
    })
});

// PUT method
app.put("/put-method", (req, res) => {
    res.status(200).json({
        message: `PUT method`
    })
});

// DELETE method
app.delete("/delete-method", (req, res) => {
    res.status(200).json({
        message: `DELETE method`
    })
});

// callback function
// la 1 function
// la 1 tham so cua 1 ham khac
// callback function se chay sau khi ham chu the duoc thuc hien
app.listen(port, () => {
    console.log("Connected to server, port:",  port);
});